package ru.tsc.pavlov.tm.enumerated;

public enum UserRole {
    USER("User"),
    ADMIN("Administrator");

    private String displayName;

    UserRole(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
