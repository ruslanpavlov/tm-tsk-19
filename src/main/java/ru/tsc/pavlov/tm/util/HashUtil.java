package ru.tsc.pavlov.tm.util;

import ru.tsc.pavlov.tm.constant.HashSalt;

public interface HashUtil {

    static String salt(String value) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < HashSalt.ITERATION; i++) {
            result = md5(HashSalt.SECRET + result + HashSalt.SECRET);
        }
        return result;
    }

    static String md5(String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
