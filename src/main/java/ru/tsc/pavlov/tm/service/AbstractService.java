package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.IRepository;
import ru.tsc.pavlov.tm.api.IService;
import ru.tsc.pavlov.tm.exception.empty.EmptyIdException;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(final E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
