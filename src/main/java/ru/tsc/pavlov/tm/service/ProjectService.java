package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.repository.IProjectRepository;
import ru.tsc.pavlov.tm.api.service.IProjectService;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.exception.empty.*;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException(name);
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project updateById(
            final String id, final String name, final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(
            final Integer index, final String name, final String description
    ) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final int index) {
        return projectRepository.existsByIndex(index);
    }

    @Override
    public Project startById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(final String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(final String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return projectRepository.changeStatusByName(name, status);
    }

}
