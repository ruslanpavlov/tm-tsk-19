package ru.tsc.pavlov.tm.api.repository;

import ru.tsc.pavlov.tm.api.IRepository;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

}
