package ru.tsc.pavlov.tm.command.auth;

import ru.tsc.pavlov.tm.constant.TerminalConst;

public class AuthLogoutCommand extends AbstractAuthCommand {

    @Override
    public String getName() {
        return TerminalConst.LOGOUT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "User logout command";
    }

    @Override
    public void execute() {
        getAuthService().logout();
    }

}
