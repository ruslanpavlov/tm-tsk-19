package ru.tsc.pavlov.tm.command.mutually;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.empty.EmptyNameException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractMutuallyCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_REMOVE_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER PROJECT NAME]");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        getProjectTaskService().removeByName(name);
        System.out.println("[OK]");
    }

}
