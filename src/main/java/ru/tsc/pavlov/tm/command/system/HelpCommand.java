package ru.tsc.pavlov.tm.command.system;

import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.constant.TerminalConst;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.HELP;
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command);
    }

}
