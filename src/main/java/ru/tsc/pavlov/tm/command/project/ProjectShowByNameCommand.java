package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_SHOW_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project by name";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = getProjectService().findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
