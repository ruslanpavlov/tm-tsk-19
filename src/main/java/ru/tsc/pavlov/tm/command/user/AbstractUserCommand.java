package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.api.service.IUserService;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.exception.entity.UserNotFoundException;
import ru.tsc.pavlov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("Middle Name: " + user.getMiddleName());
        System.out.println("Role " + user.getRole());
    }

}
