package ru.tsc.pavlov.tm.command.task;

import ru.tsc.pavlov.tm.constant.TerminalConst;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_CLEAR;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Drop all tasks";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
        System.out.println("[OK]");
    }

}
