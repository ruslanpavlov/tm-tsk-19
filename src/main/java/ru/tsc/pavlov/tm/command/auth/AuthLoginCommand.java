package ru.tsc.pavlov.tm.command.auth;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class AuthLoginCommand extends AbstractAuthCommand {

    @Override
    public String getName() {
        return TerminalConst.LOGIN;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "User login command";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
