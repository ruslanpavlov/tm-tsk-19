package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.IProjectRepository;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public boolean existsByIndex(final int index) {
        return index < list.size() && index >= 0;
    }

    @Override
    public boolean existsByName(final String name) {
        return findByName(name) != null;
    }

    @Override
    public Project findByName(final String name) {
        for (final Project project : list)
            if (name.equals(project.getName())) return project;
        return null;
    }

    @Override
    public Project findByIndex(final int index) {
        return list.get(index);
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project startById(final String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(final String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(final String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(final String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}
